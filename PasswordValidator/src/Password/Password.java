package Password;

/**
 * 
 * @author taran
 *VALIDATES PASSWORD AND IS BEING DEVELOPED USING TDD
 *return true if the characters is 8 or more . No spaces are allowed
 */

public class Password {
	private static int MIN_LENGTH=8;
	public static boolean isValidLength(String password) {
		
		
		return password.indexOf(" ")<0 && password.trim().length()>=MIN_LENGTH;
		
	}

	public static boolean isValidDigits(String password) {
		int len=password.length();
		int digits=0;
		for(int i=0;i<len;i++) {
			if(password.charAt(i)=='0'||password.charAt(i)=='1'||password.charAt(i)=='2'||password.charAt(i)=='3'||
					password.charAt(i)=='4'||password.charAt(i)=='5'||password.charAt(i)=='6'||password.charAt(i)=='7'
					||password.charAt(i)=='8'||password.charAt(i)=='9')
				digits++;
		}
		if(digits<2) {
           return false;		
          }
		else
		     return true;
	}
}
